package rtnl

import (
	"fmt"
	"net"
	// "strings"

	"github.com/mdlayher/netlink"
	log "github.com/sirupsen/logrus"
	"golang.org/x/sys/unix"
)

// common struct parsing

// for types with N uint32 fields
type xU32 struct {
	field []uint32
}

// parse
func parseXU32(cnt int, ad *netlink.AttributeDecoder) (*xU32, error) {
	v := &xU32{field: make([]uint32, cnt)}
	ad.Do(func(b []byte) error {
		if len(b) < cnt*4 {
			return fmt.Errorf("data is short")
		}
		for i := 0; i < cnt; i++ {
			v.field[i] = ad.ByteOrder.Uint32(b[(i * 4):(i*4 + 4)])
		}
		return nil
	})
	return v, ad.Err()
}

type iflaVfMac struct {
	Vfn uint32
	Mac []byte // 32 bytes allocd for this
}

func (ivm *iflaVfMac) parse(ad *netlink.AttributeDecoder) error {
	ad.Do(func(b []byte) error {
		if len(b) < 36 {
			return fmt.Errorf("IFLA_VF_MAC is short")
		}
		ivm.Vfn = ad.ByteOrder.Uint32(b[0:4])
		ivm.Mac = b[4:10]
		return nil
	})
	return ad.Err()
}

func parseIflaVfInfo(vfInfos *VfInfos, data []byte) error {
	ad, err := netlink.NewAttributeDecoder(data)
	if err != nil {
		return fmt.Errorf("failed to create vf info decoder - %s", err.Error())
	}
	if vfInfos.Map == nil {
		vfInfos.Map = make(map[uint32]*VfInfo)
	}
	m := vfInfos.Map
	var vfi *VfInfo
	var ok bool
	for ad.Next() {
		switch ad.Type() {
		case unix.IFLA_VF_MAC:
			var ivm iflaVfMac
			err = ivm.parse(ad)
			if err != nil {
				return err
			}
			if vfi, ok = m[ivm.Vfn]; !ok {
				vfi = new(VfInfo)
				m[ivm.Vfn] = vfi
			}
			vfi.Address = net.HardwareAddr(ivm.Mac)

		case unix.IFLA_VF_BROADCAST:
			log.Debug("parseIflaVfInfo: ignoring IFLA_VF_BROADCAST")

		case unix.IFLA_VF_VLAN:
			v, err := parseXU32(3, ad)
			if err != nil {
				return fmt.Errorf("error parsing IFLA_VF_VLAN: %s", err.Error())
			}
			Vfn := v.field[0]
			Vlan := v.field[1]
			Qos := v.field[2]
			if vfi, ok = m[Vfn]; !ok {
				vfi = new(VfInfo)
				m[Vfn] = vfi
			}
			vfi.Vlan = Vlan
			vfi.Qos = Qos

		case unix.IFLA_VF_RATE:
			v, err := parseXU32(3, ad)
			if err != nil {
				return fmt.Errorf("error parsing IFLA_VF_RATE: %s", err.Error())
			}
			Vfn := v.field[0]
			MinTxRate := v.field[1]
			MaxTxRate := v.field[2]
			if vfi, ok = m[Vfn]; !ok {
				vfi = new(VfInfo)
				m[Vfn] = vfi
			}
			vfi.MinTxRate = MinTxRate
			vfi.MaxTxRate = MaxTxRate

		case unix.IFLA_VF_TX_RATE:
			v, err := parseXU32(2, ad)
			if err != nil {
				return fmt.Errorf("error parsing IFLA_VF_TX_RATE: %s", err.Error())
			}
			Vfn := v.field[0]
			TxRate := v.field[1]
			if vfi, ok = m[Vfn]; !ok {
				vfi = new(VfInfo)
				m[Vfn] = vfi
			}
			vfi.TxRate = TxRate

		case unix.IFLA_VF_SPOOFCHK:
			v, err := parseXU32(2, ad)
			if err != nil {
				return fmt.Errorf("error parsing IFLA_VF_SPOOFCHK: %s", err.Error())
			}
			Vfn := v.field[0]
			Spoofchk := v.field[1]
			if vfi, ok = m[Vfn]; !ok {
				vfi = new(VfInfo)
				m[Vfn] = vfi
			}
			vfi.Spoofchk = (Spoofchk != 0)

		case unix.IFLA_VF_LINK_STATE:
			log.Debug("parseIflaVfInfo: ignoring IFLA_VF_LINK_STATE")
		case unix.IFLA_VF_RSS_QUERY_EN:
			log.Debug("parseIflaVfInfo: ignoring IFLA_VF_RSS_QUERY_EN")
		case unix.IFLA_VF_TRUST:
			log.Debug("parseIflaVfInfo: ignoring IFLA_VF_TRUST")
		case unix.IFLA_VF_VLAN_LIST:
			log.Debug("parseIflaVfInfo: ignoring IFLA_VF_VLAN_LIST")
		case unix.IFLA_VF_STATS:
			log.Debug("parseIflaVfInfo: ignoring IFLA_VF_STATS")
		default:
			log.Debugf("parseIflaVfInfo: unknown code x%02x", ad.Type())
		}
	}
	return nil
}

func (l *Link) SetVf(ctx *Context, vfn uint32, vfi *VfInfo) error {
	err := l.Read(ctx)
	if err != nil {
		return err
	}

	ae := netlink.NewAttributeEncoder()
	ae.Nested(unix.IFLA_VFINFO_LIST, func(nae *netlink.AttributeEncoder) error {
		nae.Nested(unix.IFLA_VF_INFO, func(nnae *netlink.AttributeEncoder) error {
			// vlan & qos
			b := make([]byte, 12)
			nnae.ByteOrder.PutUint32(b[0:4], vfn)
			nnae.ByteOrder.PutUint32(b[4:8], vfi.Vlan)
			nnae.ByteOrder.PutUint32(b[8:12], vfi.Qos)
			nnae.Bytes(unix.IFLA_VF_VLAN, b)
			// mac
			b = make([]byte, 36)
			nnae.ByteOrder.PutUint32(b[0:4], vfn)
			copy(b[4:], vfi.Address)
			nnae.Bytes(unix.IFLA_VF_MAC, b)

			// spoofchk
			b = make([]byte, 8)
			nnae.ByteOrder.PutUint32(b[0:4], vfn)
			spoofchk := uint32(0)
			if vfi.Spoofchk {
				spoofchk = 1
			}
			nnae.ByteOrder.PutUint32(b[4:8], spoofchk)
			nnae.Bytes(unix.IFLA_VF_SPOOFCHK, b)

			// txrate, didn't work, and blew the whole operation

			// log.Printf("txrate = %d", vfi.TxRate)
			// b = make([]byte, 8)
			// nnae.ByteOrder.PutUint32(b[0:4],  vfn)
			// nnae.ByteOrder.PutUint32(b[4:8],  vfi.TxRate)
			// nnae.Bytes(unix.IFLA_VF_TX_RATE, b)

			_, err := nnae.Encode()
			return err
		})
		_, err := nae.Encode()
		return err
	})

	attrs, err := ae.Encode()
	if err != nil {
		return err
	}

	m := netlink.Message{
		Header: netlink.Header{
			Type:  netlink.HeaderType(unix.RTM_SETLINK),
			Flags: netlink.Request | netlink.Acknowledge,
		},
		Data: append(IfInfomsgBytes(l.Msg), attrs...),
	}

	return netlinkUpdate(ctx, []netlink.Message{m})
}
