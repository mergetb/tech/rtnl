package rtnl

import (
	"github.com/mdlayher/netlink"
	"golang.org/x/sys/unix"
)

const (
	IFLA_VLAN_UNSPEC uint16 = iota
	IFLA_VLAN_ID
	IFLA_VLAN_FLAGS
	IFLA_VLAN_EGRESS_QOS
	IFLA_VLAN_INGRESS_QOS
	IFLA_VLAN_PROTOCOL
)

type Vlan struct {
	Id   uint16
	Link uint32
}

func (v *Vlan) Marshal(ctx *Context) ([]byte, error) {

	ae := netlink.NewAttributeEncoder()
	ae.Uint32(unix.IFLA_LINK, v.Link)
	ae.Do(unix.IFLA_LINKINFO, func() ([]byte, error) {

		ae1 := netlink.NewAttributeEncoder()
		ae1.Bytes(IFLA_INFO_KIND, []byte("vlan"))
		ae1.Do(IFLA_INFO_DATA, func() ([]byte, error) {

			ae2 := netlink.NewAttributeEncoder()
			ae2.Uint16(IFLA_VLAN_ID, v.Id)

			return ae2.Encode()

		})

		return ae1.Encode()

	})

	return ae.Encode()

}

func (v *Vlan) Unmarshal(ctx *Context, buf []byte) error {

	ad, err := netlink.NewAttributeDecoder(buf)
	if err != nil {
		return err
	}

	for ad.Next() {
		switch ad.Type() {
		case IFLA_VLAN_ID:
			v.Id = ad.Uint16()
		}
	}

	return nil

}

// Resolve handle attributes
func (v *Vlan) Resolve(ctx *Context) error {

	return nil

}
