package rtnl

import (
	"encoding/json"
	"net"
	"os"
	"os/exec"
	"reflect"
	"strings"
	"testing"
)

type IProute2Link struct {
	Link   string
	Ifname string
}

func Test_AddVeth(t *testing.T) {

	ctx, err := OpenDefaultContext()
	if err != nil {
		t.Fatal(err)
	}
	defer ctx.Close()

	out, err := exec.Command("ip", "netns", "add", "donkey").CombinedOutput()
	if err != nil {
		t.Log(string(out))
		t.Fatal(err)
	}
	defer func() {
		out, err = exec.Command("ip", "netns", "del", "donkey").CombinedOutput()
		if err != nil {
			t.Log(string(out))
			t.Fatal(err)
		}
	}()

	tctx, err := OpenContext("donkey")
	if err != nil {
		t.Fatal(err)
	}
	defer tctx.Close()

	ctx.Target = tctx

	// add a veth link
	ve := &Link{
		Info: &LinkInfo{
			Name: "vethB",
			Ns:   uint32(tctx.Fd()),
			Veth: &Veth{
				Peer: "vethA",
			},
		},
	}
	err = ve.Add(ctx)
	t.Logf("%+v", ve.Info)
	if err != nil {
		t.Fatal(err)
	}
	ve.Info.Veth.ResolvePeer(ctx)
	if ve.Info.Veth.Peer != "vethA" {
		t.Fatal("peer lost")
	}

	// ensure iproute2 sees it and parameters are correct
	out, err = exec.Command(
		"ip", "-j", "link", "show", "dev", "vethA",
	).CombinedOutput()
	if err != nil {
		t.Fatal(string(out))
	}

	var ilinks []IProute2Link
	err = json.Unmarshal(out, &ilinks)
	if err != nil {
		t.Fatal(err)
	}

	if len(ilinks) == 0 {
		t.Fatal("created veth not found")
	}

	if ilinks[0].Ifname != "vethA" {
		t.Fatal("veth does not have correct name")
	}

	/*
		if ilinks[0].Link != "vethA" {
			t.Log(ilinks[0].Link)
			t.Fatal("veth peer does not have correct name")
		}
	*/

	// read back and ensure peer equality

	lnk, err := GetLink(ctx, "vethA")
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("%+v", lnk.Info)
	lnk.Info.Veth.ResolvePeer(tctx)
	if ve.Info.Name != lnk.Info.Veth.Peer {
		t.Fatalf("peer of read link not correct %v != %v",
			ve.Info.Veth.Peer, lnk.Info.Veth.Peer,
		)
	}
	/*
		if ve.Info.Address.String() != lnk.Info.Address.String() {
			t.Fatalf("L2 address mismatch %s != %s",
				ve.Info.Address.String(),
				lnk.Info.Address.String(),
			)
		}
	*/
	if err != nil {
		t.Fatal(err)
	}

	err = ve.Present(ctx)
	if err != nil {
		t.Fatal(err)
	}

	err = ve.Del(tctx)
	if err != nil {
		t.Fatal(err)
	}

	err = ve.Absent(tctx)
	if err != nil {
		t.Fatal(err)
	}

}

func Test_VethNamespace(t *testing.T) {

	ctx, err := OpenDefaultContext()
	if err != nil {
		t.Fatal(err)
	}
	defer ctx.Close()

	va := &Link{
		Info: &LinkInfo{
			Name: "vethA",
			Veth: &Veth{
				Peer: "vethB",
			},
		},
	}
	err = va.Add(ctx)
	if err != nil {
		t.Fatal(err)
	}

	out, err := exec.Command("ip", "netns", "add", "pizza").CombinedOutput()
	if err != nil {
		t.Fatal(string(out))
	}

	f, err := os.Open("/var/run/netns/pizza")
	if err != nil {
		t.Fatalf("failed to open netns file: %v", err)
	}
	nsfd := f.Fd()

	vb := &Link{
		Info: &LinkInfo{
			Name: "vethB",
			Ns:   uint32(nsfd),
		},
	}
	err = vb.Set(ctx)
	if err != nil {
		t.Fatal(err)
	}

	// ensure iproute2 sees the link in correc namespace
	out, err = exec.Command(
		"ip", "netns", "exec", "pizza", "ip", "-j", "link", "show", "dev", "vethB",
	).CombinedOutput()
	if err != nil {
		t.Fatal(string(out))
	}

	var ilinks []IProute2Link
	err = json.Unmarshal(out, &ilinks)
	if err != nil {
		t.Fatal(err)
	}

	if len(ilinks) == 0 {
		t.Fatal("created veth not found")
	}

	if ilinks[0].Ifname != "vethB" {
		t.Fatal("veth does not have correct name")
	}

	// note that we cannot test for the peer here because it is in a different
	// namespace

	// cleanup

	va.Del(ctx)

	out, err = exec.Command("ip", "netns", "del", "pizza").CombinedOutput()
	if err != nil {
		t.Fatal(string(out))
	}

}

func Test_VethAddress(t *testing.T) {

	ctx, err := OpenDefaultContext()
	if err != nil {
		t.Fatal(err)
	}
	defer ctx.Close()

	va := &Link{
		Info: &LinkInfo{
			Name: "vethA",
			Veth: &Veth{
				Peer: "vethB",
			},
		},
	}
	err = va.Add(ctx)
	if err != nil {
		t.Fatal(err)
	}

	addr, err := ParseAddr("192.168.47.1/24")
	if err != nil {
		t.Fatal(err)
	}

	err = va.AddAddr(ctx, addr)
	if err != nil {
		t.Fatal(err)
	}

	err = va.Up(ctx)
	if err != nil {
		t.Fatal(err)
	}

	vb, err := GetLink(ctx, "vethB")
	if err != nil {
		t.Fatal(err)
	}

	err = vb.Up(ctx)
	if err != nil {
		t.Fatal(err)
	}

	err = va.Del(ctx)
	if err != nil {
		t.Fatal(err)
	}

}

func Test_Bridge(t *testing.T) {

	ctx, err := OpenDefaultContext()
	if err != nil {
		t.Fatal(err)
	}
	defer ctx.Close()

	br := &Link{
		Info: &LinkInfo{
			Name:   "br47",
			Bridge: &Bridge{},
		},
	}
	err = br.Present(ctx)
	if err != nil {
		t.Fatal(err)
	}

	err = br.Promisc(ctx, true)
	if err != nil {
		t.Fatal(err)
	}

	err = br.Up(ctx)
	if err != nil {
		t.Fatal(err)
	}

	brr, err := GetLink(ctx, "br47")
	if err != nil {
		t.Fatal(err)
	}

	if !brr.Info.Promisc {
		br.Del(ctx)
		t.Fatal("promisc readback failed")
	}

	addr, _ := ParseAddr("1.2.3.4/24")
	err = br.AddAddr(ctx, addr)
	if err != nil {
		br.Del(ctx)
		t.Fatal(err)
	}

	va := &Link{
		Info: &LinkInfo{
			Name:   "vethA",
			Master: uint32(br.Msg.Index),
			Veth: &Veth{
				Peer: "vethB",
			},
		},
	}
	err = va.Add(ctx)
	if err != nil {
		t.Fatal(err)
	}

	err = va.Del(ctx)
	if err != nil {
		t.Fatal(err)
	}

	err = br.Del(ctx)
	if err != nil {
		t.Fatal(err)
	}

}

func Test_Vxlan(t *testing.T) {

	ctx, err := OpenDefaultContext()
	if err != nil {
		t.Fatal(err)
	}
	defer ctx.Close()

	lo, err := GetLink(ctx, "lo")
	if err != nil {
		t.Fatal(err)
	}

	vx := &Link{
		Info: &LinkInfo{
			Name: "vtep47",
			Vxlan: &Vxlan{
				Vni:     47,
				DstPort: 4789,
				Local:   net.ParseIP("1.2.3.4"),
				Link:    uint32(lo.Msg.Index),
			},
		},
	}

	err = vx.Add(ctx)
	if err != nil {
		t.Fatal(err)
	}

	xv, err := GetLink(ctx, "vtep47")
	if err != nil {
		t.Error(err)
	}
	if err == nil {
		if xv.Info.Vxlan == nil {
			t.Error("no vxlan data")
		}
		if !reflect.DeepEqual(*xv.Info.Vxlan, *vx.Info.Vxlan) {
			t.Error("vxlans do not match")
			t.Logf("expected: %#v", *vx.Info.Vxlan)
			t.Logf("actual: %#v", *xv.Info.Vxlan)
		}
	}

	err = vx.Del(ctx)
	if err != nil {
		t.Fatal(err)
	}

}

func Test_Wg(t *testing.T) {

	ctx, err := OpenDefaultContext()
	if err != nil {
		t.Fatal(err)
	}
	defer ctx.Close()

	ctx, err = OpenDefaultContext()
	if err != nil {
		t.Fatal(err)
	}

	// create wg link

	lnk := &Link{
		Info: &LinkInfo{
			Name:      "wgtest",
			Wireguard: &Wireguard{},
		},
	}

	err = lnk.Add(ctx)
	if err != nil {
		t.Fatal(err)
	}
	defer lnk.Absent(ctx)

	// fetch wg link

	wl, err := GetLink(ctx, "wgtest")
	if err != nil {
		t.Fatal(err)
	}

	if wl.Info.Wireguard == nil {
		t.Error("expected wireguard link")
	}

}

func checkAltNames(t *testing.T, link string, expected []string) {
	ctx, err := OpenDefaultContext()
	if err != nil {
		t.Fatal(err)
	}
	defer ctx.Close()
	l, err := GetLink(ctx, link)
	if err != nil {
		t.Fatal(err)
	}
	if strings.Join(l.Info.AltNames, ",") != strings.Join(expected, ",") {
		t.Fatalf("Unexpected altnames: %v", l.Info.AltNames)
	}
}

func Test_AltName(t *testing.T) {
	const TESTLNK = "goat"
	out, err := exec.Command("ip", "link", "add", "name", TESTLNK, "type", "dummy").CombinedOutput()
	if err != nil {
		t.Log(string(out))
		t.Fatal(err)
	}
	defer func() {
		out, err = exec.Command("ip", "link", "del", TESTLNK).CombinedOutput()
		if err != nil {
			t.Log(string(out))
			t.Fatal(err)
		}
	}()

	ctx, err := OpenDefaultContext()
	if err != nil {
		t.Fatal(err)
	}
	defer ctx.Close()

	l, err := GetLink(ctx, TESTLNK)
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("l.Info=%#v", l.Info)

	// first - add, then delete some
	names := []string{"aaaa", "bbbb", "cccc"}
	err = l.AddAltNames(ctx, names)
	if err != nil {
		t.Fatal(err)
	}
	checkAltNames(t, TESTLNK, names)
	err = l.DelAltNames(ctx, []string{"bbbb"})
	if err != nil {
		t.Fatal(err)
	}
	checkAltNames(t, TESTLNK, []string{"aaaa", "cccc"})

	// test lookup by alt name
	l, err = GetLink(ctx, "aaaa")
	if err != nil {
		t.Fatal(err)
	}
	if l.Info.Name != TESTLNK {
		t.Errorf("lookup by altname didn't match: expected `%s`, got `%s`", TESTLNK, l.Info.Name)
	}
}

func Test_SetVfVlan(t *testing.T) {
	link := os.Getenv("IFACE_WITH_VFS")
	if link == "" {
		t.Skip("Skipping test, set IFACE_WITH_VFS environment, to enable this test, make sure Vf0 exists and is `disposable`")
	}
	ctx, err := OpenDefaultContext()
	if err != nil {
		t.Fatal(err)
	}
	defer ctx.Close()

	l, err := GetLink(ctx, link)
	if err != nil {
		t.Fatal(err)
	}

	vfn := uint32(0)

	// save a copy to restore
	vfi_saved := *l.Info.Vfi.Map[vfn]
	defer func() {
		err = l.SetVf(ctx, vfn, &vfi_saved)
		if err != nil {
			t.Logf("vf state restore attempt failed: %s", err.Error())
		}
	}()

	// t.Logf("l=%#v", l.Info)
	address, _ := net.ParseMAC("88:11:22:33:44:55")
	vfi := &VfInfo{
		Vlan:     777,
		Qos:      1,
		Address:  address,
		Spoofchk: false,

		// doesn't work for some hardware, test by running e.g.
		// sudo ip link set dev enp130s0f0 vf 2 rate 10
		// TxRate: 100,
	}
	err = l.SetVf(ctx, vfn, vfi)
	if err != nil {
		t.Fatal(err)
	}
	l, err = GetLink(ctx, link)
	if err != nil {
		t.Fatal(err)
	}
	// check if fetched data matches
	vfiNew := l.Info.Vfi.Map[vfn]
	if vfi.Vlan != vfiNew.Vlan {
		t.Errorf("Vlan wasn't set: set %d, actual %d", vfi.Vlan, vfiNew.Vlan)
	}
	if vfi.Qos != vfiNew.Qos {
		t.Errorf("Qos wasn't set: set %d, actual %d", vfi.Qos, vfiNew.Qos)
	}
	if vfi.Address.String() != vfiNew.Address.String() {
		t.Errorf("Mac wasn't set: set %s, actual %s", vfi.Address.String(), vfiNew.Address.String())
	}
	if vfi.Spoofchk != vfiNew.Spoofchk {
		t.Errorf("Spoofchk wasn't set: set %v, actual %v", vfi.Spoofchk, vfiNew.Spoofchk)
	}
}
