package main

import (
	"log"
	"strconv"

	"github.com/spf13/cobra"

	"gitlab.com/mergetb/tech/rtnl"
)

func vlanCommands(root *cobra.Command) {

	vlan := &cobra.Command{
		Use:   "vlan",
		Short: "vlan command family",
	}
	root.AddCommand(vlan)

	add := &cobra.Command{
		Use:   "add <device> <name> <id>",
		Short: "add vlan",
		Args:  cobra.ExactArgs(3),
		Run: func(cmd *cobra.Command, args []string) {

			vid, err := strconv.Atoi(args[2])
			if err != nil {
				log.Fatal(err)
			}

			addvlan(args[0], args[1], uint16(vid))

		},
	}
	vlan.AddCommand(add)

}

func addvlan(dev, name string, id uint16) {

	ctx, err := rtnl.OpenDefaultContext()
	if err != nil {
		log.Fatal(err)
	}

	target, err := rtnl.GetLink(ctx, dev)
	if err != nil {
		log.Fatal(err)
	}

	link := rtnl.NewLink()
	link.Info.Name = name
	link.Info.Vlan = &rtnl.Vlan{
		Link: uint32(target.Msg.Index),
		Id:   id,
	}

	err = link.Add(ctx)
	if err != nil {
		log.Fatal(err)
	}

}
