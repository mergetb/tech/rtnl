module gitlab.com/mergetb/tech/rtnl

go 1.15

require (
	github.com/fatih/color v1.7.0
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/mattn/go-colorable v0.1.1 // indirect
	github.com/mdlayher/netlink v1.7.2
	github.com/sirupsen/logrus v1.3.0
	github.com/spf13/cobra v0.0.3
	github.com/spf13/pflag v1.0.3 // indirect
	golang.org/x/sys v0.7.0
)
